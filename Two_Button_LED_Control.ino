// Connect a capacitor with a capacitance between 100uF and 1000uF
// from power to ground to smooth out the power supply.
// Add a 220 or 470 Ohm resistor between the Arduino digital output pin
// and the strip data input pin to reduce noise on that line.

// buttons are objects created from the Button class:
#include "Button_class.h"
#include "Palettes.h"

// ---------------- Button Read Declarations ------------------
const int ledOnboard = 13; // the pin that the LED is attached to
int LEDState = LOW;    // current state of the button
int click_count;       // for counting clicks
int wait_time = 1500;  // milliseconds to wait for all the clicks to be counted before responding
long send_time;        // after waiting, the specific time to send click count

Button buttonTop(2);     // we instantiate the buttons with pin numbers
Button buttonBottom(4);

// ------------------------- setup ---------------------------------------
void setup()
{
  delay(3000); // power-up safety delay

  // instantiate buttons and click count
  buttonTop.begin();
  buttonBottom.begin();
  click_count = 0;

  // define LED output pin
  pinMode(ledOnboard, OUTPUT);
  // sets up live feedback in serial monitor
  Serial.begin(9600);

  // FastLED setup
  FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(BRIGHTNESS);
}




// ------------------------- loop ---------------------------------------

void loop()
{
  long now = millis();

  // -------------- check button status ---------------------
  int clicks1 = buttonTop.buttonChanged();
  int clicks2 = buttonBottom.buttonChanged();

  if (clicks1)
  {
    click_count = clicks1;
    send_time = now + wait_time;
  }
  else if (clicks2)
  {
    click_count = clicks2;
    send_time = now + wait_time;
  }

  // If we have a click to report, and the waiting time
  // has passed, we send it to the response method.
  if (click_count && now >= send_time) {

    button_response();
    click_count = 0;
  }

  // -------- cycle LEDS if turned on -----------------
  if (LEDState == HIGH) {
    runCurrentPalette();
  }
}



// --------------------   custom functions ---------------------

// this determines which path to take depending
// on how many clicks were received on a button
void button_response() {

  // if LEDs are off, any number of clicks just turns them on
  if (LEDState == LOW) {
    click_count = 1;
  }

  // button logic
  if (click_count == 1)
  {
    // for a single click we just toggle the LEDs on/off
    Serial.println("Single Click");
    toggle_LED();
  }
  else if (click_count == 2)
  {
    // double click moves forward in the palette options
    Serial.println("Double Click");
    current_palette_num++;
    setCurrentPalette();
  }
  else if (click_count == 3)
  {
    // a triple click toggles the brightness from daymode(bright)/nightmode(dim)
    Serial.println("Triple Click");
    toggle_brightness();
  }
  else if (click_count >= 4)
  {
    // quadra click moves forward in the palette options
    Serial.println("Quadra Click");
    current_palette_num--;
    setCurrentPalette();
  }
}




// just turns the LEDs on/off ------------------------
void toggle_LED()
{
  if (LEDState == LOW)
  {
    digitalWrite(ledOnboard, HIGH);
    LEDState = HIGH;

    // Turn LEDS on
    setCurrentPalette();
  }
  else
  {
    digitalWrite(ledOnboard, LOW);
    LEDState = LOW;

    // Turn LEDs Off by setting them all to black
    SetupAllBlack();
    runCurrentPalette();
  }
}


// allows for day mode (bright) or night mode (dim) -------------------
void toggle_brightness() {

  if (nightModeEnabled) {
    nightModeEnabled = false;
    currentBrightness = dayModeBrightness;
  } else {
    nightModeEnabled = true;
    currentBrightness = nightModeBrightness;
  }
}


// this assigns palettes that can be cycled
// through by double/quadra clicks
void setCurrentPalette() {

  int number_of_options = 5;

  // cycle palette
  if (current_palette_num > number_of_options) {
    current_palette_num = 1;
  } else if (current_palette_num < 1) {
    current_palette_num = number_of_options;
  }

  Serial.println("Palette Number:");
  Serial.println(current_palette_num);
  Serial.println();

  if (current_palette_num == 1) {

    currentPalette = myRedWhiteBluePalette_p;
    currentBlending = LINEARBLEND;

  } else if (current_palette_num == 2) {

    currentPalette = RainbowColors_p;
    currentBlending = LINEARBLEND;

  } else if (current_palette_num == 3) {

    currentPalette = HeatColors_p;
    currentBlending = NOBLEND;

  } else if (current_palette_num == 4) {

    currentPalette = ForestColors_p;
    currentBlending = LINEARBLEND;

  } else if (current_palette_num == 5) {

    currentPalette = CloudColors_p;
    currentBlending = LINEARBLEND;
  }
}



// -------------------- fills LED strip with colours ------------------------
void runCurrentPalette()
{
  static uint8_t startIndex = 0;
  startIndex = startIndex + 1; // motion speed
  FillLEDsFromPaletteColors(startIndex, currentBrightness);
  FastLED.show();
  FastLED.delay(1000 / UPDATES_PER_SECOND);
}

void FillLEDsFromPaletteColors(uint8_t colorIndex, uint8_t brightness)
{
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = ColorFromPalette(currentPalette, colorIndex, brightness, currentBlending);
    colorIndex += 3;
  }
}
