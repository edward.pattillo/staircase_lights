// this class defines a button object
// we instantiate it with the pin number, eg:
// Button button1(2);

class Button
{

  private:
    bool _state;
    uint8_t _pin;
    int _debounceDelay;

    long _lastButtonPressTime;
    int _waitTime;
    int _clickCount;


  public:
    Button(uint8_t pin) : _pin(pin) {} // object is instantiated with pin #

    void begin()
    {
      pinMode(_pin, INPUT_PULLUP);     // init button
      _state = digitalRead(_pin);      // read the state of the button
      _clickCount = 0;                 // count the clicks
      _lastButtonPressTime = millis(); // initialise button press time
      _waitTime = 1000;                // milliseconds to wait for next button press
      _debounceDelay = 50;             // milliseconds to delay for debounce effect

      delay(_debounceDelay);           // delay to "debounce" button
    }

    int buttonChanged()
    { // detects if the button has changed state

      bool v = digitalRead(_pin);

      if (v != _state)
      { // compares current state to previously recorded state

        _state = v; // records this state

        // create timing to see if it's a multi-click
        long now = millis();
        long interval = now - _lastButtonPressTime; // checks the difference between this press and the previous one
        _lastButtonPressTime = now; // resets last button press to current time
        
        if (interval > _waitTime)
        {
          // reset click count
          _clickCount = 1;
        }
        else
        {
          _clickCount++;
        }

        Serial.println(_clickCount);
        return _clickCount;
      }
      return 0;
    }
};
